(function(){

    "use strict";

    angular
        .module('myProject.route')
        .config(configRoute);

    configRoute.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configRoute($stateProvider,$urlRouterProvider){

        $stateProvider
            .state('myProject',{ 
            abstract : true,
            views : {
                '': { template : ' <div ui-view="header"></div> <div ui-view="menu"></div><div ui-view></div>'},
                'header@myProject': { templateUrl : 'js/app/layout/header/header.html',
                                      controller : 'HeaderCtrl',
                                      controllerAs : 'headerVm'},
                'menu@myProject': { templateUrl : 'js/app/layout/menu/menu.html',
                                    controller : 'MenuCtrl',
                                    controllerAs : 'menuVm'}
            }
        })

            .state('myProject.main', {
            url: '/home',
            controller : 'mainCtrl',
            controllerAs : 'mainCtrlVm',
            templateUrl: 'js/app/module/home/home.html'
        });

        $urlRouterProvider.otherwise('/home');

    }       
})();