(function () {

    "use strict";
	
    /** Declarations des modules de l'application */

    angular
        .module('myProject',  [
        'ngMaterial',
        'ngMessages',
        'ngAria',
        'ngAnimate',
        'ui.router',
        'myProject.shared',  
        'myProject.layout',
        'myProject.route',
        'myProject.modules'
    ])

    angular.module('myProject.shared',['ngStorage']);
    angular.module('myProject.layout',[]);
    angular.module('myProject.route',[]);
    angular.module('myProject.modules',[]);
    
})();